# Gitlab-apiからの取得データ
apiからのissue情報取得時に必要なデータを記載します。

###  必要なデータ
- Title
- Description
- Asignee
- due_date
- iid
  - プロジェクト内部で割り振れられるid。
- project_id
  -  プロジェクト識別id。プロジェクトの識別のため必要。
### 備考
Date:2022-11-30
