# devcontainer

### 概要
開発環境(コンテナ)を用意してvscode上で弄れる。  
環境は一度設定ファイルを用意すればあとはvscodeを開くだけで構築が完了する。

### 必要なもの
- Dockerfile
- devcontainer.json 
  - Dockerfileの情報、コンテナにインストールする拡張機能、VSCodeの設定等を定義 

### 環境構築(sample)

- ディレクトリ構成
```
.
└── issuectl
    ├── .devcontainer
    │   ├── Dockerfile
    │   └── devcontainer.json
```
Dockerfile
```
FROM python:3.9.16-slim

RUN pip install --no-cache-dir \
    black \
    flake8 \
    isort \
    mypy \
    pytest
```
devcontainer.json
```
{
	//コンテナ名
	"name": "python_remote",
	//指定するDockerfile
	"build": {
		"dockerfile": "Dockerfile"
	},

	// devcontainer側のvscodeの設定一覧
	"settings": {
		"terminal.integrated.shell.linux": "/bin/bash",
        "python.pythonPath": "/usr/local/bin/python",
		"files.insertFinalNewline": true,
		"files.trimTrailingWhitespace": true,
		"python.formatting.provider": "black",
		"python.formatting.blackArgs": [
			"--line-length=79 "
		],
		"python.linting.flake8CategorySeverity.E": "Warning",
		"python.linting.flake8CategorySeverity.F": "Warning",
		"python.sortImports.path": "/usr/local/bin/isort",
		"editor.codeActionsOnSave": {
			"source.organizeImports": true
		},
		"[json]": {
			"editor.defaultFormatter": "vscode.json-language-features"
		}
	},

	// devcontainer側にインストールするvscodeの拡張機能一覧
	"extensions": [
		"usernamehw.errorlens",
		"oderwat.indent-rainbow",
		"shardulm94.trailing-spaces",
		"njpwerner.autodocstring"
	],
}
```

- 起動手順  
1. vscode画面左下をクリック  
2. メニューより「Remote-Containers: Open Folder in Container...」をクリック

