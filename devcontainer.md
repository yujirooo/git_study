# UnitTest

### UnitTestとは
- クラスやメソッドの単位を対象として行うテスト。
- テストが自動化されている
- 開発者自身が行う

### UnitTestの目的や考え方
[ユニットテストにまつわる10の勘違い](https://dev.classmethod.jp/articles/10_errors_about_unit_testing/)

### PythonにおけるUnitTest
主に使われているフレームワークが以下２つ。  
- UnitTests  
pythonの標準ライブラリ

- pytest  
pipでインストール可能
